package org.abbtech.notification.model;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Entity
@Setter
@Getter
@Builder
@AllArgsConstructor
@Table(name = "notif")
public class Notification {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "messages")
    private String Messages;
    @Column(name = "useremail")
    private String userEmail;
    @Column(name = "userid")
    private UUID userId;
    @Column(name = "ordersid")
    private UUID orderId;

    public Notification() {

    }
}
