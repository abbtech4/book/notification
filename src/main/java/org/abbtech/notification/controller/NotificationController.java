package org.abbtech.notification.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.abbtech.notification.dto.NotificationDTO;
import org.abbtech.notification.service.NotificationService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/notification")
@RequiredArgsConstructor
@Tag(name = "notification",description = "reads data from broker and " +
        "only writes to db. No other job, no meaning this endpoint just for documentation(Only USER role people can access it")
public class NotificationController {

    private final NotificationService notificationService;

    @Operation(summary = "sends notification")
    @PostMapping("/send")
    public ResponseEntity<NotificationDTO> sendNotification(@RequestBody NotificationDTO notificationDTO,
                                                            @RequestHeader(name = "x-user-id") String userId,
                                                            @RequestHeader(name = "x-user-email") String userEmail){
        return new ResponseEntity<>(notificationService.sendNotification(notificationDTO,userId,userEmail), HttpStatus.CREATED);
    }
}
