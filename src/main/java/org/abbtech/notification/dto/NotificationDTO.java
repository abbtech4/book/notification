package org.abbtech.notification.dto;

import lombok.Getter;

import java.util.UUID;

@Getter
public class NotificationDTO {
    private String message;
    private UUID orderId;
}
