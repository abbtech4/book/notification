package org.abbtech.notification.dto;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;

import java.util.UUID;

@Builder
@Getter
@Data
public class KafkaDTO {
    private UUID orderId;
    private String messages;
    private UUID userId;
    private String userEmail;
}
