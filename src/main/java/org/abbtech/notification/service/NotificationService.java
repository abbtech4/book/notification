package org.abbtech.notification.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.abbtech.notification.dto.KafkaDTO;
import org.abbtech.notification.dto.NotificationDTO;
import org.abbtech.notification.model.Notification;
import org.abbtech.notification.repository.NotificationRepository;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;


import java.util.UUID;

@Service
@RequiredArgsConstructor
public class NotificationService {
    private final NotificationRepository notificationRepository;
    private final EmailService emailService;
    private final ObjectMapper objectMapper;

    @KafkaListener(topics = "NotificationTopic",groupId = "ilyas")
    void listener(String data){

        try {
            KafkaDTO kafkaDTO = objectMapper.readValue(data, KafkaDTO.class);
           Notification notification = Notification.builder()
                    .Messages(kafkaDTO.getMessages())
                            .userEmail(kafkaDTO.getUserEmail())
                                    .userId(kafkaDTO.getUserId())
                                            .orderId(kafkaDTO.getOrderId())
                                                    .build();
            notificationRepository.save(notification);
            emailService.sendEmail(notification.getUserEmail(),"confirm order","http://localhost:8083/order/confirm/"+notification.getMessages().split(": ")[1]);
            System.out.println(kafkaDTO);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public NotificationDTO sendNotification(NotificationDTO notificationDTO, String userId,String userEmail){
        Notification notification = Notification.builder()
                .Messages(notificationDTO.getMessage())
                .orderId(notificationDTO.getOrderId())
                .userId(UUID.fromString(userId))
                .userEmail(userEmail)
                .build();
        notificationRepository.save(notification);
        return notificationDTO;
    }
}
